import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue } from 'bootstrap-vue'
import Vuelidate from 'vuelidate'

Vue.use(BootstrapVue)
Vue.use(Vuelidate)
Vue.config.productionTip = false

import '@/assets/css/style.scss'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
