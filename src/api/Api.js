import axios from 'axios'

export default {
    url: process.env.VUE_APP_API_BASE,
    getSectors() {
        return axios.get(this.url + '/sectors', {
            params: {
                mode: "tree"
            },
            withCredentials: true
        })
    },
    save(name, sectorIds, agreedWithTerms) {
        return axios.post(this.url + '/users', {
            name, sectorIds, agreedWithTerms
        }, {withCredentials: true})
    },
    loadLast() {
        return axios.get(this.url + '/users?latest=true', {withCredentials: true})
    }
}
